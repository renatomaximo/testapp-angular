myApp.controller('empController', function($route,$scope,$http,$routeParams){
	$scope.getProducts = function(){
		$http.post('http://127.0.0.1:8000/api/v1/products/getList').then(function(response){
			$scope.products = response.data['result'];
		});
	};
	$scope.addProduct = function(info){
		$http.post('http://127.0.0.1:8000/api/v1/products/create', info).then(function(response){
			window.location.href = 'http://localhost/angular/';
		});
	};
	$scope.showProduct = function(){
		var id = $routeParams.id;
		$http.get('http://127.0.0.1:8000/api/v1/products/get/'+id).then(function(response){
			var emp  = response.data['result'];
			$scope.product = emp[0];
		});
	};
	$scope.updateProduct = function(info){
		$http.patch('http://127.0.0.1:8000/api/v1/products/update', info).then(function(response){
			console.log(response);
			window.location.href = 'http://localhost/angular/';
		});
	};
	$scope.deleteProduct = function(id){
		var id = id;
		$http.delete('http://127.0.0.1:8000/api/v1/products/delete/'+id).then(function(response){
			$route.reload();
		});
	};

});
