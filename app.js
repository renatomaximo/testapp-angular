var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function($routeProvider){
	$routeProvider
	.when('/', {
		templateUrl:'templates/list.html',
		controller:'empController'
	})
	.when('/products', {
		templateUrl:'templates/list.html',
		controller:'empController'
	})
	.when('/products/create', {
		templateUrl:'templates/create.html',
		controller:'empController'
	})
	.when('/products/:id/edit', {
		templateUrl:'templates/edit.html',
		controller:'empController'
	})
	.when('/products/:id/show', {
		templateUrl:'templates/show.html',
		controller:'empController'
	});
});

